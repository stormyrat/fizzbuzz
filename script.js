function fizzBuzz(maxValue) {
    for (let x = 1; x <= maxValue; x++) {
        if (x % 2 == 0 & x % 3 == 0)
            console.log("FizzBuzz");
        else if (x % 2 == 0)
            console.log("Fizz");
        else if (x % 3 == 0)
            console.log("Buzz");
        else
            console.log(x);
    }
    return maxValue;
}
console.log(fizzBuzz(12))

function fizzBuzzPrime (maxValue) {
    for (let i = 1; i <= maxValue; i++){
        if (i % 2 !== 0 && i % 3 !== 0 && i % 5 !== 0 && i % 7 !== 0 && i % 9 !== 0 || i === 3|| i === 5|| i === 7|| i === 9) {
            console.log(i + ' Prime')
        }
        else {
            console.log(i)
        }
    }
}
fizzBuzzPrime(40)